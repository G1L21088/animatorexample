package mx.gilsantaella.animatorexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

public class MainActivity extends AppCompatActivity {

    Button btnGone;
    LinearLayout loadingPanel;
    ProgressBar progressBar;
    private int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        onGone();
    }

    private void onGone(){
        btnGone = (Button) findViewById(R.id.btnGone);
        progressBar = (ProgressBar) findViewById(R.id.progessLoading);
        btnGone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i = progressBar.getVisibility();
                if(i == 0) {
                    progressBar.setVisibility(View.GONE);
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}
